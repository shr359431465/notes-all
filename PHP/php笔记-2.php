<?php

/**
*	函数技巧
*/	

# json_encode($arr,JSON_UNESCAPED_UNICODE);		//第二个参数可以让转json字符串时不转义中文

# 三元运算符	$expr=$expr1 ?: $expr2;		如果expr1结果为True,则返回expr1的结果		

# checkdnsrr()
	使用checkdnsrr()通过域名存在性来确认部分email地址的有效性

# memory_get_usage()函数查看当前内存消耗：
	PHP性能优化过程中避免不了需要获取PHP内存消耗，使用memory_get_usage()函数可获取当前的内存消耗情况
	要想减少内存的占用，可以使用 PHP unset() 函数把不再需要使用的变量删除。类似的还有：PHP mysql_free_result() 函数，可以清空不再需要的查询数据库得到的结果集
	
# set_time_limit(0);   设置程序执行时间
	括号里边的数字是执行时间，如果为零说明永久执行直到程序结束，如果为大于零的数字，则不管程序是否执行完成，到了设定的秒数，程序结束。 

# substr_count($str1,$str2);  
	统计子字符串（$str2）在原字符串($str1)中出现的次数
	
# PHP的filter函数可以用来进行验证。  
	filter_var()函数，通过改变参数可以验证email的是否正确;
	
# 定界符<<<		对于需要直接输出的html，可以使用定界符
$str = <<< EOF
    Here is your string 
    {$data}
EOF;
	定界符内的php变量一定要像这种{$a}模式，否则不会识别。
	
/**
*	通用规则、优化
*/	

# 当需要判断字符串长度是否大于某个值时，可以用isset代替strlen，可以提高执行效率
		if(!isset($str{5})) 比 if(strlen($str)>5) 更好

# $i++会比++$i慢一些

# 不要随便就复制变量
	复制变量，结果是增加了一倍的内存消耗

# 内容压缩	
	几乎所有的浏览器都支持Gzip的压缩方式，gzip可以降低80%的输出，付出的代价是大概增加了10%的cpu计算量
	你可以在PHP.ini中开启它
　	zlib.output_compression = On
　	zlib.output_compression_level = (level)(level可能是1-9之间的数字，你可以设置不同的数字使得他适合你的站点。）
	
# 单双引号
	用单引号代替双引号来包含字符串，因为PHP会在双引号包围的字符串中搜寻变量，单引号则不会。

# 开启nginx服务器的页面压缩。	相对于php的压缩，服务器压缩效率更高

