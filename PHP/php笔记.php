<?php
	//字符串函数
	一。可以代替正则判断
	1.ctype_alnum   检测是否是只包含[A-Za-z0-9]
	2.ctype_alpha   检测是否是只包含[A-Za-z]
	3.ctype_digit 	检查时候是只包含数字字符的字符串（0-9）
	4.ctype_lower 	检查是否所有的字符都是英文字母，并且都是小写的
	5.ctype_upper 	检查是否所有的字符都是英文字母，并且都是大写的
	
	二。对传递的get参数进行字符串编码
	1.urlencode($str)	对字符串进行编码
	2.urldecode($str)	解码已经编码后的字符串
	
	三。防止sql注入的方法
	1.htmlspecialchars		对提交的数据进行转义处理（将预定义字符转为html实体）
	2.htmlspecialchars_decode		对从数据库中获取到的转义后的html实体数据进行反转义
	3.addslashes		使用反斜线对预定义字符进行转义
	4.stripslashes		对使用addslashes转义后的字符进行反转义
	
	四。字符串函数
	1.str_pad 			对字符串两侧进行补白（默认为右填充）
	2.str_repeat		重复输出某个字符串
	3.str_split 		将一个字符串按照字符间距分割成数组
	4.strstr/stristr(不区分大小写)			查找字符串首次出现的位置（返回该位置及其之后的字符串。若第三个参数为true，则返回该位置之前的字符串）
	
	五。防止模拟提交
	1.防止用户模拟ajax提交：若提交的数据来源于ajax提交，则在http头信息中会保存XMLHttpRequest来代表为ajax提交的数据
							通过$_SERVER['HTTP_X_REQUESTED_WITH']（只有提交方法为ajax提交时，才会有这个字段）获取到其中的值（XMLHttpRequest），进而可以验证数据的来源
	2.防止站外提交：可以通过获取$_SERVER['HTTP_REFERER']（其中保存的数据为数据来源地址）的值来判断数据来源
	
	3.获取访问者ip:function getIP(){//记录来访者的IP信息
						if(getenv('HTTP_CLIENT_IP')){
							$ip=getenv('HTTP_CLIENT_IP');
						}else if(getenv('HTTP_X_FORWARDED_FOR')){
							$ip=getenv('HTTP_X_FORWARDED_FOR');
						}else if(getenv('REMOTE_ADDR')){
							$ip=getenv('REMOTE_ADDR');
						}else{
							$ip=$_SERVER['REMOTE_ADDR'];
						}
						return $ip;
					}
	
	六。php函数（数组和对象相关）
	1.php对象转数组       get_object_vars(); //返回由对象属性组成的关联数组
	2.把数组中的一部分去掉并用其它值取代    array_splice();  //第四个参数为用来替换的数据
	3.过滤数组中的单元		array_filter();		//可以用来过滤数组中的空数据
	
	七。相关实例接口
	1.获取qq用户的头像：<img src="http://q1.qlogo.cn/g?b=qq&nk=359431465&s=100">			nk参数为qq名称
	
	八。.当类成员属性被声明为private时，必须以双下划线"__"开头；被声明为protected时，必须以单下划线"_"开头；声明为public的成员属性则在任何时候都不允许含有下划线。

	九。字符编码类型转换
	iconv('GB2312', 'UTF-8', $str); //将字符串的编码从GB2312转到UTF-8 
	
	十。自定义错误
		set_error_handler("my_error3",E_USER_WARNING);		//第一个参数为自定义错误所调用的函数名，第二个参数为报错等级。	当报错等级为E_USER_WARNING时会调用my_error3函数
		
		错误触发器：trigger_error("输入的年龄过大",E_USER_WARNING);
		
		
	十一。curl函数简单使用
		//初始化
		$ch = curl_init();
		//设置选项
		curl_setopt($ch,CURLOPT_URL,"http://www.0531yun.cn/wsjc/Device/getDeviceData.do?userID=171205hccx&userPassword=171205hccx");
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);		//设置内容不直接输出
		curl_setopt($ch,CURLOPT_HEADER,0);				//设置不返回http头信息
		
		//设置为post请求
		// $arr = array("userID"=>"171205hccx","userPassword"=>"171205hccx");
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $arr);
		// curl_setopt($ch, CURLOPT_POST,true);
		
		// 执行并获取HTML文档内容
		$output = curl_exec($ch);
		//释放资源
		curl_close($ch);
		
		//获取各种运行中信息，便于调试    
		$curlInfo = curl_getinfo($ch);
		
	十二。用于将byte进行转换
		function calc($size,$digits=2){ 
		  $unit= array('','K','M','G','T','P');
		  $base= 1024;
		  $i = floor(log($size,$base));
		  $n = count($unit);
		  if($i >= $n){
			$i=$n-1;
		  }
		  return round($size/pow($base,$i),$digits).' '.$unit[$i] . 'B';
		}