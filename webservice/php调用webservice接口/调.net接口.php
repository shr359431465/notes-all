<?php
	//1.首先你的PHP要支持SOAP
	//2.WebService接口数据调用
	ini_set("soap.wsdl_cache_enabled",0);  //关闭缓存
    libxml_disable_entity_loader(false);//这一句可以提高webservice服务的可靠性  
    $soap=new SoapClient('http://131.107.100.230/rfidTest/WebService/RFIDWebService.asmx?wsdl');    //这里填写你要调用的URL
    $soap->soap_defencoding = 'utf-8';
    $soap->decode_utf8 = false;
    $soap->xml_encoding = 'utf-8';

	//$data = $soap->__getFunctions();		获取接口中所有的对外函数
	//调用必须用__soapCall，参数：第一个位要调用的函数名，第二个为该函数要传递的参数数组，返回值为一个对象
	$ParamData = array('userID'=>"www.shinow.com.cn",'userPassword'=>"nc.moc.wonihs.www",'bloodIDs'=>array("4943239000000000613","3943239000000000704"));
	$p = $soap->__soapCall('SelectBloodByIdList',array('parameters' => $ParamData));
	$array = json_decode(json_encode($p),true);		//将对象转换成一个数组		
	
	