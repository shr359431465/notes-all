<?php

class getData
{
	public $title = [
		'wadat_ist' => '实际交货日期',
		'kunag' => '售达方编号',
		'matnr' => '物料编号',
		'vtext1' => '基本单位',
		'menge_pcs' => '出货数量',
		'kbetr_zj' => '出货金额',
		// 'abcd' => '物料种类',
		// 'abcdx' => '物料种类描述',
	];
	public function get($postFilter)
	{
		foreach ($postFilter as $item) {
			if($item['name'] == 'wadat_ist'&&$item['value']){
		      	list($filter['wadat_ist|bthan'],$filter['wadat_ist|sthan']) = explode("-", $item['value']);
				$filter['wadat_ist|bthan'] = str_replace('/', '-', trim($filter['wadat_ist|bthan']));
        			$filter['wadat_ist|sthan'] = str_replace('/', '-', trim($filter['wadat_ist|sthan']));
			}
			if($item['name'] == 'abcd'&&$item['value']){
				$filter['abcd'] = $item['value'];
			}
			if($item['name'] == 'kunag'&&$item['value']){
				$filter['kunag'] = $item['value'];
				$filter['kunag'] = str_pad($filter['kunag'], 10,'0',STR_PAD_LEFT);
			}
			if($item['name'] == 'vkorg'&&$item['value']){
				$filter['vkorg'] = $item['value'];
				$vkorg = $item['value'];
			}
			if($item['name'] == 'wbstk'&&$item['value']){
				$filter['wbstk'] = $item['value'];
			}
		}
		
		$pdo = new PDO('mysql:dbname=bill;host=127.0.0.1','root','');
        	$sql = "select * from systrade_delivery_bill where abcd='".$filter['abcd']."' and kunag='".$filter['kunag']."' and vkorg='".$filter['vkorg']."' and wbstk='".$filter['wbstk']."' and wadat_ist between '".$filter['wadat_ist|bthan']."' and '".$filter['wadat_ist|sthan']."' order by wadat_ist asc,matnr asc";        	
        	$sth = $pdo->prepare($sql);
        	$sth->execute();
        	$data = $sth->fetchAll(PDO::FETCH_ASSOC);
	      $array = array();

		if($data){
			foreach ($data as $key => $v) {
		      	$array[$key]['wadat_ist'] = $v['wadat_ist'];
		      	$array[$key]['kunag'] = $v['kunag'];
		 
		      	$array[$key]['matnr'] = $v['matnr'];
		      	
		      	$array[$key]['vtext1'] = $v['vtext1'];
		      	$array[$key]['menge_pcs'] = $v['menge_pcs'];
		      	$array[$key]['kbetr_zj'] = $v['kbetr_zj'];
		      	if($vkorg=='1000')
		      	{
			      	$array[$key]['abcd'] = $v['abcd'];
			      	$array[$key]['abcdx'] = $v['abcdx'];
			    }
			    else
			    {
			    	$array[$key]['wgbez'] = $v['wgbez'];
			    }
		     }
		}else{
			$array = array();
		}
		$result = [];
		if($vkorg == '1000')
		{
			$this->title['abcd'] = '物料种类';
			$this->title['abcdx'] = '物料种类描述';
		}
		else
		{
			$this->title['wgbez'] = '系列名';
		}
		$result[] = $this->title;
		
		$result = array_merge($result,$array);
		return $result;
	}
}