<?php
	class download{

		public function fileDownload($fileName, $fileType, $filter)
		{
			$filetypeObj = new xls();
			$dataObject = new getData();

			$filetypeObj->set_queue_header($fileName.'.'.$fileType);
			if( method_exists($filetypeObj, 'setBom') )
			{
				$bom = $filetypeObj->setBom();
				echo $bom;
			}

			
		      $data = $dataObject->get($filter);

		      $this->export($fileType, $data);
		}

		private function export($filetype, $data)
		{
			//实例化导出文件类型类
			$filetypeObj = new xls();

			//加入文件头部数据
			$fileHeader = $filetypeObj->fileHeader();
			if( $fileHeader )
			{
			echo $fileHeader;
			}

			//导出数据写到本地文件
			$rs = $filetypeObj->arrToExportType($data);
			echo $rs;

			if( !$rs )
			{
			echo  '数据为空';
			}

			//加入文件尾部数据
			$fileFoot = $filetypeObj->fileFoot();
			if( $fileFoot )
			{
			echo $fileFoot;
			}
		}
	}
?>