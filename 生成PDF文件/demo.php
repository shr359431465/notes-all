<?php
	include("./mpdf/mpdf.php");		//引用生成pdf文件的类库
	set_time_limit(0);		//设置程序执行结束时间
	
	//$mode,$format,默认字体大小，默认字体，左页边距25（默认），右页边距（25），上页边距16，下页边距16，mgh:16,mgf:13,orientation  
	$mpdf=new mPDF('utf-8','A4','','',10,10,16,16); //'utf-8' 或者 '+aCJK' 或者 'zh-CN'都可以显示中文 
	
	//设置字体，解决中文乱码  
	$mpdf -> useAdobeCJK = TRUE;  
	$mpdf ->autoScriptToLang = true;  
	$mpdf -> autoLangToFont = true;  
	
	//$mpdf-> showImageErrors = true; //显示图片无法加载的原因，用于调试，注意的是,我的机子上gif格式的图片无法加载出来。
	
	//设置pdf显示方式  
	$mpdf->SetDisplayMode('fullpage');
	
	//目录相关设置：  
	//Remember bookmark levels start at 0(does not work inside tables)H1 - H6 must be uppercase  
	//$this->h2bookmarks = array('H1'=>0, 'H2'=>1, 'H3'=>2);  
	//$mpdf->h2toc = array('H3'=>0,'H4'=>1,'H5'=>2);  
	//$mpdf->h2bookmarks = array('H3'=>0,'H4'=>1,'H5'=>2);  
	//$mpdf->mirrorMargins = 1;  
	//是否缩进列表的第一级  
	//$mpdf->list_indent_first_level = 0; 
	
	
	//设置PDF页眉内容
	$header = "页眉<hr>";
	//设置PDF页脚内容
	$footer = "<hr>页脚";
	//添加页眉和页脚到pdf中
	$mpdf->SetHTMLHeader($header);
	$mpdf->SetHTMLFooter($footer);
	
	
	//导入外部css文件：  
	//$stylesheet1 = file_get_contents('./target.css');  
	//$mpdf->WriteHTML($stylesheet1,1);  
	
	//导入外部html内容文件
	//$html = file_get_contents('./target.html');  
	$html = "<body><div style='padding-top:10px;'>你好</div></body>";
	$mpdf->WriteHTML($html);  //$html中的内容即为变成pdf格式的html内容。  
	
	//用于下载文件的名称定义
	$fileName = 'XXXX报告.pdf';  
	//输出pdf文件  
	$mpdf->Output($fileName,'I'); //'I'表示在线展示 'D'则显示下载窗口 