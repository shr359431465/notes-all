<?php
    /**
     *  单列模式
     *  单例对象的类必须保证只有一个实例存在
     *  构造函数和析构函数必须声明为私有,防止外部程序new 类从而失去单例模式的意义
     *  $_instance必须声明为静态的私有变量
     *  getInstance()方法必须设置为公有的,必须调用此方法 以返回实例的一个引用
     *  私有的__clone()方法防止克隆对象
     */
    class Uni
    {
        static private $_instance;
        private function __construct()
        {
            echo "实例化";
        }

        private function __clone()
        {
            // TODO: Implement __clone() method.
        }

        static public function getInstance(){
            if(!static::$_instance){
                static::$_instance = new Uni();
            }
            return static::$_instance;
        }

        public function getConfig(){

        }
    }

    /**
     *  工厂模式
     *  避免当改变某个类的名字或者方法之后，在调用这个类的所有的代码中都修改它的名字或者参数
     */
    class Factory{
        /*
         * 如果某个类在很多的文件中都new ClassName()，那么万一这个类的名字
         * 发生变更或者参数发生变化，如果不使用工厂模式，就需要修改每一个PHP
         * 代码，使用了工厂模式之后，只需要修改工厂类或者方法就可以了。
         */
        static function createDatabase(){
            $classObj = Uni::getInstance();
            return $classObj;
        }
    }

    /**
     *  适配器模式--策略模式（感觉有点混淆）
     *  给不同的函数接口封装统一的API,给上层提供统一的API
     *  PHP中的数据库操作有MySQL,MySQLi,PDO三种，可以用适配器模式统一成一致，使不同的数据库操作，统一成一样的API
     */
    interface Target
    {
        public function sampleMethod1();

        public function sampleMethod2();
    }

    class Mysqlc implements Target
    {
        public function sampleMethod1()
        {
            // TODO: Implement sampleMethod1() method.
            echo "mysqlc";
        }

        public function sampleMethod2()
        {
            // TODO: Implement sampleMethod2() method.
        }
    }

    class Mysqlic implements Target
    {
        public function sampleMethod1()
        {
            // TODO: Implement sampleMethod1() method.
            echo "mysqlic";
        }

        public function sampleMethod2()
        {
            // TODO: Implement sampleMethod2() method.
        }
    }


    class Demo implements Target
    {
        public $db;
        public function __construct($type)
        {
            $this->db = new $type;
        }
        public function sampleMethod1()
        {
            return $this->db->sampleMethod1();
        }

        public function sampleMethod2()
        {
            return $this->db->sampleMethod2();
        }
    }

    /**
     *  观察者模式（不是很理解）
     */
