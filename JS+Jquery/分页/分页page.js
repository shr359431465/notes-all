/*
*   function        pageSet     分页样式
*   param           page        当前页
*   param           total       总页数
*   return          pageHtml    分页Html
*   description     仅用于实现样式，具体点击功能事件自写。该方法目前针对ajax方法，a链接跳转方法暂时未添加。
* */
function pageSet(page,total) {
    var pageHtml = "";
    if(total > 1){
        pageHtml = '<nav aria-label="Page navigation pull-right">\n' +
            '                <ul class="pagination"><li><a href="#" data="1">首页</a></li>';
        if(page == 1){
            pageHtml += '<li class="disabled">\n' +
                '\t\t\t\t\t\t\t\t\t\t\t<a href="#" data="0" aria-label="Previous">\n' +
                '\t\t\t\t\t\t\t\t\t\t\t\t<span aria-hidden="true">&laquo;</span>\n' +
                '\t\t\t\t\t\t\t\t\t\t\t</a>\n' +
                '\t\t\t\t\t\t\t\t\t\t</li>';
        }else{
            pageHtml += '<li>\n' +
                '\t\t\t\t\t\t\t\t\t\t\t<a href="#"  data="'+ (page-1) +'" aria-label="Previous">\n' +
                '\t\t\t\t\t\t\t\t\t\t\t\t<span aria-hidden="true">&laquo;</span>\n' +
                '\t\t\t\t\t\t\t\t\t\t\t</a>\n' +
                '\t\t\t\t\t\t\t\t\t\t</li>';
        }
        if(total > 7){
            if(page <= 1){
                for(var i = 1;i <= 7;i++){
                    if(page == i){
                        pageHtml += '<li class="active"><a href="#" data="'+ i +'">'+ i +'</a></li>';
                    }else{
                        pageHtml += '<li><a href="#" data="'+ i +'">'+ i +'</a></li>';
                    }
                }
            }else if(page > 1){
                if(page - 3 <= 0){
                    var i = 1;
                }else{
                    var i = page - 3 ;
                }
                var count = 0;
                for(i;i <= page;i++){
                    if(page == i){
                        pageHtml += '<li class="active"><a href="#" data="'+ i +'">'+ i +'</a></li>';
                    }else{
                        pageHtml += '<li><a href="#" data="'+ i +'">'+ i +'</a></li>';
                    }
                    count++;
                }
                var totalCount = page + (7 - count);
                if(totalCount > total){
                    totalCount = total;
                }
                for(var j = page+1;j <= totalCount;j++){
                    pageHtml += '<li><a href="#" data="'+ j +'">'+ j +'</a></li>';
                }
            }
        }else{
            for(var i = 1;i <= total;i++){
                if(page == i){
                    pageHtml += '<li class="active"><a href="#" data="'+ i +'">'+ i +'</a></li>';
                }else{
                    pageHtml += '<li><a href="#" data="'+ i +'">'+ i +'</a></li>';
                }
            }
        }
        if(page == total){
            pageHtml += '<li class="disabled">\n' +
                '\t\t\t\t\t\t\t\t\t\t\t<a href="#" data="0" aria-label="Next">\n' +
                '\t\t\t\t\t\t\t\t\t\t\t\t<span aria-hidden="true">&raquo;</span>\n' +
                '\t\t\t\t\t\t\t\t\t\t\t</a>\n' +
                '\t\t\t\t\t\t\t\t\t\t</li>';
        }else{
            pageHtml += '<li>\n' +
                '\t\t\t\t\t\t\t\t\t\t\t<a href="#" data="'+ (page+1) +'" aria-label="Next">\n' +
                '\t\t\t\t\t\t\t\t\t\t\t\t<span aria-hidden="true">&raquo;</span>\n' +
                '\t\t\t\t\t\t\t\t\t\t\t</a>\n' +
                '\t\t\t\t\t\t\t\t\t\t</li>';
        }
        pageHtml += '<li><a href="#" data="'+ total +'">尾页</a></li></ul>\n' +
            '        </nav>';
        return pageHtml;
    }else{
        return pageHtml;
    }
}
