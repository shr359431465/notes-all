<?php
    class Unit{
        //创建静态私有的变量保存该类对象
        static private $_instance = null;
        //参数
        private $config;
        //防止直接创建对象
        public function __construct(){
            if (!self::$_instance) {
                self::$_instance = new RedisCluster(NULL, ['127.0.0.1:9001', '127.0.0.1:9002', '127.0.0.1:9003', '127.0.0.1:9004', '127.0.0.1:9005','127.0.0.1:9006']);		//使用redis集群
                //self::$_instance->connect("127.0.0.1",9001,1);
				self::$_instance->->auth('20160601');		//设置登陆密码
                //echo "我被实例化了";
            }
            return self::$_instance;

        }
        //防止克隆对象
        private function __clone(){

        }

        public function set(){
            $arr = array(
                "bar"=>'aaa',
                'test'=>'bbb',
                'arr'=>array(
                    "a"=>1,
                    "b"=>2
                )
            );
            self::$_instance->mset($arr);
        }

        public function get(){
           return self::$_instance->get("name");
        }

        public function append(){
            self::$_instance->append("bar","123");
        }

        public function setList(){
            self::$_instance->rPush("barder","ddd","123");
        }

        public function hset(){
            self::$_instance->hset("hashtest","bbb","123");
        }
    }
    $unit = new Unit();
    //$unit->hset();
    $a = $unit->get();
    print_r($a);
//    $a = $unit->append();
//    $a = $unit->get();
//    print_r($a);
