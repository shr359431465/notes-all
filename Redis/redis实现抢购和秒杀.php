<?php
	/**
	 * 使用redis实现高并发下 的秒杀和抢购
	 * 根据返回结果$result进行判断是否抢购成功
	 */
	header('content-type:text/html;charset=utf-8');
	$redis = new redis();
	$redis->connect('127.0.0.1',6379);
	//$redis->flushall();
	$total = 10;		//总共可抢购的数量
	$userid = 1;		//获取用户的唯一标识
	$usertotal = $redis->get('usertotal');
	if($usertotal < $total){   	//先判断抢购数量
		$res = $redis->sIsMember('userlist',$userid);
		if(!$res){
			$redis->watch('usertotal');		//判断该用户是否已经参加过抢购
			//sleep(5);
			$redis->multi();
			$redis->set('usertotal',$usertotal + 1);
			$redis->sadd('userlist',$userid);
			$result = $redis->exec();
		}else{
			echo '已经参加过抢购';
		}
	}else{
		echo "抢购已结束";
	}
	error_log(''.var_export($result,1)."\r\n",3,__FILE__.'.log');
