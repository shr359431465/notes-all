<?php
	//载入ucpass类
	require_once('lib/Ucpaas.class.php');

	//初始化必填
	$options['accountsid']='c205041491d563e7630c417622201438';
	$options['token']='c509f2ec3e1efe9f2f19540e5ce38dec';


	//初始化 $options必填
	$ucpass = new Ucpaas($options);

	//开发者账号信息查询默认为json或xml

	$ucpass->getDevinfo('xml');

	//语音验证码,云之讯融合通讯开放平台收到请求后，向对象电话终端发起呼叫，接通电话后将播放指定语音验证码序列
	$appId = "01dc17acfa714ff6826b47b137a57e6b";
	$verifyCode = $_GET['code'];
	$to = $_GET['mobile'];

	$data = $ucpass->voiceCode($appId,$verifyCode,$to);
	$error = json_decode($data,true);
	return $error['resp']['respCode'];
