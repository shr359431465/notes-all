<?php
	/*
	**	websocket数据通信基础版
	**	仅进行了简单的客户端连接，模拟向客户端推送数据
	**	需要当作一个单独的服务，通过命令行启动
	*/
	use Workerman\Connection\AsyncTcpConnection;
	use Workerman\Worker;
	require_once './Workerman/Autoloader.php';

	$clients = []; //保存客户端信息，用于存储所有来自客户端的connection连接对象
	
	// 初始化一个worker容器，用于监听服务器1234端口，作为客户端数据监听端口
	$worker = new Worker('websocket://192.168.131.9:1234');
	// 这里进程数必须设置为1
	$worker->count = 1;

	// worker进程启动后建立一个内部通讯端口
	$worker->onWorkerStart = function($worker)
	{
	    // 开启一个内部端口，方便内部系统推送数据，Text协议格式 文本+换行符，用于开启一个接收后台向前台推送的数据
	    $inner_text_worker = new Worker('Text://192.168.131.9:5678');

	    //用于接收内部端口发送的数据
	    $inner_text_worker->onMessage = function($connection, $buffer)
	    {
	        global $worker;
	        // $data数组格式，里面有uid，表示向那个uid的页面推送数据
	        $data = json_decode($buffer, true);

	       	$uid = $data['uid'];
	        
	        // // 通过workerman，向uid的页面推送数据
	        $ret = sendMessageByUid($uid, "aaaa");
	        // // 返回推送结果，向服务器后台返回处理结果
	        $connection->send("ok");
	    };
	    $inner_text_worker->listen();
	};

	// 当有客户端发来消息时执行的回调函数，用于接收客户端发送来的数据，并对客户端进行反馈
	$worker->onMessage = function($connection, $data)
	{  
	   	global $clients;	//使用全局变量

	   	$ip = $connection->getRemoteIp();	//获取客户端的IP地址

	   	$clients[$ip] = $connection;	//将客户端的连接对象根据相应的IP进行数据存储
	    
	    $connection->send('notice:success'); //向客户端发送的验证成功消息	
	};

	//向客户端推送数据函数
	function sendMessageByUid($uid, $message)
	{
	    global $clients;

	    $clients[$uid]->send("notice:".$message);

	}


	// 运行所有的worker（其实当前只定义了一个）
	Worker::runAll();