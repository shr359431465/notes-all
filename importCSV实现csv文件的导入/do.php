<?php

/**
 * @
 * @Description:
 * @Copyright (C) 2011 helloweba.com,All Rights Reserved.
 * -----------------------------------------------------------------------------
 * @author: Suohaoran
 * @Create: 2012-5-1
 * @Modify:
*/
//include_once ("connect.php");

$action = $_GET['action'];
if ($action == 'import') { //导入CSV
	$filename = $_FILES['file']['tmp_name'];
	if (empty ($filename)) {
		echo '请选择要导入的CSV文件！';
		exit;
	}
	$handle = fopen($filename, 'r');
	$result = input_csv($handle); //解析csv
	$len_result = count($result);
	if($len_result==0){
		echo '没有任何数据！';
		exit;
	}
	for ($i = 0; $i < $len_result; $i++) { //循环获取各字段值
		$data_values[$i][] = iconv('gb2312', 'utf-8', $result[$i][0]); //中文转码
		$data_values[$i][] = iconv('gb2312', 'utf-8', $result[$i][1]);
		$data_values[$i][] = $result[$i][2];	
	}
	fclose($handle); //关闭指针
	return $data_values;   //返回要插入的数据组成的数组
	
}
function input_csv($handle) {
	$out = array ();
	$n = 0;
	while ($data = fgetcsv($handle, 10000)) {
		$num = count($data);
		for ($i = 0; $i < $num; $i++) {
			$out[$n][$i] = $data[$i];
		}
		$n++;
	}
	return $out;
}

?>
